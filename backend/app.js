const path = require("path");
const express = require("express");
const pjson = require("./package.json");

const PORT = process.env.PORT || 3001;

const app = express();

// Node s'occupe de servir les fichiers statiques
app.use(express.static(path.resolve(__dirname, "../frontend/build")));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization, X-Access-Token"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

// Handle GET requests to /api route
app.get("/api", (req, res) => {
  const msg = `Hello from server ! Version : ${pjson.version}`;
  res.json({ message: msg });
  console.log(msg);
});

// Tout ce qui n'a pas été traité par le backend correspond à un fichier statique
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../frontend/build", "index.html"));
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
